import React from 'react'
import {Container} from 'react-bootstrap'
function Post(props) {
    let id = props.match.params.id
    return (
        <div>
            <Container>
                <h3>This is component from post {id}</h3>
            </Container>
        </div>
    )
}

export default Post
