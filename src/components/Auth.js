import React from 'react'
import { Form, Button, } from 'react-bootstrap';
import { Container } from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Auth() {
    return (
        <div>
            <Container>
                <Form>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>UserName</Form.Label>
                        <Form.Control type="text" placeholder="Enter Username" />
                    </Form.Group>

                    <Form.Group controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="Password" />
                    </Form.Group>
                    <Link as={Link} to="/Welcome">
                        <Button variant="primary" type="submit">
                            Submit
                        </Button>
                    </Link>
                </Form>
            </Container>
        </div>
    );
}
