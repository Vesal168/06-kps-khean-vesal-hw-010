
import React, { Component } from "react";
import CardItem from "./CardItem";
export default class Home extends Component {
    constructor() {
        super();
        this.state = {
            Users: [
                {
                    id: "1",
                    name: "Spider Man",
                    des:
                        "Some quick example text to build on the card title and make up the bulk of the card's content.",
                    img:
                        "https://upload.wikimedia.org/wikipedia/en/f/f9/Spider-Man_Homecoming_poster.jpg",
                },
                {
                    id: "2",
                    name: "Advengure EndGame",
                    des:
                        "Some quick example text to build on the card title and make up the bulk of the card's content.",
                    img:
                        "https://cccadvocate.com/wp-content/uploads/2019/05/Endgame.jpg",
                },
                {
                    id: "3",
                    name: "Sweet Love",
                    des:
                        "Some quick example text to build on the card title and make up the bulk of the card's content.",
                    img:
                        "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTQaVtzbb7zOj53Q2aXddGL69bCDOoQQebk9QnQYPxtKOHwBgwq&usqp=CAU",
                },
            ],
        };
    }
    render() {
        return (
                
            <div className="container">
                <div className="row">
                    {this.state.Users.map((item, index) => (
                        <CardItem key={item.id} name={item.name} des={item.des} id={item.id} />
                    ))}
                </div>
            </div>
        );
    }
}
