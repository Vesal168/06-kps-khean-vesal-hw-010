import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useParams,
    useRouteMatch
} from "react-router-dom";
import { Container } from "react-bootstrap";

export default function Video() {
    return (
        <Router>
            <div>
                <Container>
                    <ul>
                        <li>
                            <Link to="/animation">Animation</Link>
                        </li>
                        <li>
                            <Link to="/movie">Movie</Link>
                        </li>
                    </ul>
                    <hr />

                    <Switch>
                        <Route path="/animation">
                            <Animation />
                        </Route>
                        <Route path="/movie">
                            <Movie />
                        </Route>
                    </Switch>
                </Container>
            </div>
        </Router>
    );
}

function Animation() {

    let { path, url } = useRouteMatch();

    return (
        <div>
            <h2>Animation Categories</h2>
            <ul>
                <li>
                    <Link to={`${url}/cartoon`}>Cartoon</Link>
                </li>
                <li>
                    <Link to={`${url}/joke`}>Joke</Link>
                </li>
                <li>
                    <Link to={`${url}/tomy`}>Tomy</Link>
                </li>
            </ul>

            <Switch>
                <Route exact path={path}>
                    <h3>Please select a topic.</h3>
                </Route>
                <Route path={`${path}/:topicId`}>
                    <Topic />
                </Route>
            </Switch>
        </div>
    );
}

function Movie() {

    let { path, url } = useRouteMatch();

    return (
        <div>
            <h2> Movie Categories</h2>
            <ul>
                <li>
                    <Link to={`${url}/Action`}>Action</Link>
                </li>
                <li>
                    <Link to={`${url}/Roman`}>Roman</Link>
                </li>
                <li>
                    <Link to={`${url}/Comedy`}>Comedy</Link>
                </li>
            </ul>

            <Switch>
                <Route exact path={path}>
                    <h3>Please select a topic.</h3>
                </Route>
                <Route path={`${path}/:topicId`}>
                    <Topic />
                </Route>
            </Switch>
        </div>
    );
}

function Topic() {
    let { topicId } = useParams();

    return (
        <div>
            <h3>{topicId}</h3>
        </div>
    );
}

