import React from 'react'
import { Container, Nav, Navbar, Form, FormControl, Button, Carousel } from "react-bootstrap";
import { Link } from 'react-router-dom';
import { Card, CardGroup } from 'react-bootstrap';
function Menu() {
    return (
        <div className="mb-3">
            <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                <Container>
                    <Navbar.Brand as={Link} to="/">KH-MOVIE</Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="mr-auto">
                            <Nav.Link as={Link} to="/Home">Home</Nav.Link>
                            <Nav.Link as={Link} to="/Video">Video</Nav.Link>
                            <Nav.Link as={Link} to="/Account">Account</Nav.Link>
                            <Nav.Link as={Link} to="/Auth">Auth</Nav.Link>
                        </Nav>
                        <Nav>
                            <Form inline>
                                <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                                <Button variant="outline-info">Search</Button>
                            </Form>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>

            <Carousel>
                <Carousel.Item>
                    <img style={{ height: '520px' }}
                        className="d-block w-100"
                        src="https://mumbrella.com.au/wp-content/uploads/2020/01/Jumanji-The-Next-Level-e1577919310818-800x449.jpeg"
                        alt="First slide"
                    />
                    <Carousel.Caption>
                        <h3>JumanJi</h3>

                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img style={{ height: '520px' }}
                        className="d-block w-100"
                        src="https://i.ytimg.com/vi/kbfAYeMQAzs/maxresdefault.jpg"
                        alt="Third slide"
                    />

                    <Carousel.Caption>
                        <h3>Advenger EndGame</h3>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img style={{ height: '520px' }}
                        className="d-block w-100"
                        src="https://2.bp.blogspot.com/-E7pTpjLGZro/XFgeqLlX7uI/AAAAAAAAEn8/5YHFRG51_vI92-S_rtSyPCmalakPzZlxgCLcBGAs/s1600/friendzone.jpg"
                        alt="Third slide"
                    />
                    <Carousel.Caption>
                        <h3>Friend Zone</h3>
                    </Carousel.Caption>
                </Carousel.Item>
            </Carousel>
            <Container>
                <CardGroup className="mt-5">
                    <Card>
                        <Card.Img variant="top" src="https://www.gstatic.com/tv/thumb/v22vodart/8170659/p8170659_v_v8_aa.jpg" />
                        <Card.Body>
                            <Card.Title>IP Man</Card.Title>
                            <Card.Text>
                                This is a wider card with supporting text below as a natural lead-in to
                                additional content. This card has even longer content than the first to
                                show that equal height action. Tom Holland stars as the eponymous superhero in this Marvel Comics sequel to 'Captain America: Civil War' (2016) that sees a young Peter Parker realise the full extent of his powers as Spider-Man.
                        </Card.Text>
                        </Card.Body>

                    </Card>
                    <Card>
                        <Card.Img variant="top" src="https://m.media-amazon.com/images/M/MV5BNjUzZjFlYTgtMWVmMy00NDE1LTk4YmQtNzQwOTVjODNmNGMxXkEyXkFqcGdeQXVyNTI5NjIyMw@@._V1_.jpg" />
                        <Card.Body>
                            <Card.Title>Romantic</Card.Title>
                            <Card.Text>
                                This is a wider card with supporting text below as a natural lead-in to
                                additional content. This card has even longer content than the first to
                                show that equal height action. Tom Holland stars as the eponymous superhero in this Marvel Comics sequel to 'Captain America: Civil War' (2016) that sees a young Peter Parker realise the full extent of his powers as Spider-Man.
                            </Card.Text>
                        </Card.Body>

                    </Card>
                    <Card>
                        <Card.Img variant="top" src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRLT9jC78lOfS1xiT7S8LQxKsxsuX4utRTRB61oPZE4r8nXu3Ac&usqp=CAU" />
                        <Card.Body>
                            <Card.Title>Sweet love</Card.Title>
                            <Card.Text>
                                This is a wider card with supporting text below as a natural lead-in to
                                additional content. This card has even longer content than the first to
                                show that equal height action. Tom Holland stars as the eponymous superhero in this Marvel Comics sequel to 'Captain America: Civil War' (2016) that sees a young Peter Parker realise the full extent of his powers as Spider-Man.
                        </Card.Text>
                        </Card.Body>

                    </Card>
                </CardGroup>
            </Container>
        </div>
    )
}
export default Menu
