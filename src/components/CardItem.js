import React from 'react'
import { Card, Button} from "react-bootstrap";
import {Link} from 'react-router-dom';

function CardItem(props) {
    return (
        <div className="col-md-4" key={props.id}>
          <Card style={{ width: "18rem" }}>
            <Card.Img
              variant="top"
              src="https://upload.wikimedia.org/wikipedia/en/f/f9/Spider-Man_Homecoming_poster.jpg"
            />
            <Card.Body>
              <Card.Title>{props.name}</Card.Title>
              <Card.Text>{props.des}</Card.Text>
              <Link to={`/Post/${props.id}`}>
                <Button variant="primary">see more</Button>
              </Link>
            </Card.Body>
          </Card>
          
        </div>
      );
}

export default CardItem
