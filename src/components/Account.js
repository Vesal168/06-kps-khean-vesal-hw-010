import React from 'react'
import { BrowserRouter as Router, Link, useLocation } from "react-router-dom";
import { Container } from 'react-bootstrap';

export default function Account() {
    return (
      <Router>
        <MyAccount />
      </Router>
    );
  }
function MyAccount() {
    let query = useQuery();

    return (
        <div>
            <Container>
                <h2>Accounts</h2>
                <ul>
                    <li>
                        <Link to="/Account?name=netflix">Netflix</Link>
                    </li>
                    <li>
                        <Link to="/Account?name=zillow-group">Zillow Group</Link>
                    </li>
                    <li>
                        <Link to="/Account?name=yahoo">Yahoo</Link>
                    </li>
                    <li>
                        <Link to="/Account?name=modus-create">Modus Create</Link>
                    </li>
                </ul>
                <Child name={query.get("name")} />
            </Container>
        </div>
    );
}
function useQuery() {
    return new URLSearchParams(useLocation().search);
}
function Child({ name }) {
    return (
        <div>
            {name ? (
                <h3>
                    The <code>name</code> in the query string is &quot;{name}
            &quot;
                </h3>
            ) : (
                    <h3>There is no name in the query string</h3>
                )}
        </div>
    );
}

